#include <iostream>
#include <vector>

class Vector
{
public:
    Vector() : x(8), y(3.3), z(6.6)
    {}

    Vector(int _x, float _y, double _z) : x(_x), y(_y), z(_z)
    {}

    void Show()
    {
        std::cout << "Numbrlc: " << x << ' ' << y << ' ' << z << '\n';
    }
   
    void Multiply()
    {
        std::cout << "Multiply: " << x * x + y * y + z * z << '\n';
    }

private:
    double x;
    double y;
    double z;
};

int main()
{
    Vector v;
    v.Show();
    v.Multiply();
}